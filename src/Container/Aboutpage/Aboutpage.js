import React, {useEffect, useState} from 'react';
import axiosURL from "../../AxiosURL";
import MyPage from "../../Components/MyPage/MyPage";

const Aboutpage = ({match}) => {
    const [info, setInfo] = useState([]);
    const id = match.params.id;

    useEffect(() => {
        const fetchData = async () => {
            const array = [];
            const {data} = await axiosURL.get('pages/' + id + '.json');
            array.push({...data, id});
            setInfo(array);
        }
        fetchData().catch(console.error);
    }, [id]);


    return (
        <div>
            {info.map((p) => (
                <MyPage
                    key={p.title}
                    title={p.title}
                    content={p.content}
                />
            ))}
        </div>
    );
};

export default Aboutpage;