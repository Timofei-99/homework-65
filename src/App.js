import './App.css';
import Layout from "./Components/UI/Layout/Layout";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Aboutpage from "./Container/Aboutpage/Aboutpage";
import EditPage from "./Container/EditPage/EditPage";

function App() {
    return (
        <BrowserRouter>
            <Layout>
                <div className='container'>
                    <Switch>
                        <Route path={'/pages/admin'} exact component={EditPage}/>
                        <Route path={'/pages/:id'} exact component={Aboutpage}/>
                    </Switch>
                </div>
            </Layout>
        </BrowserRouter>
    );
}

export default App;
