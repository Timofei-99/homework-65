import axios from "axios";

const axiosURL = axios.create({
    baseURL: 'https://pages-5e5ed-default-rtdb.firebaseio.com/'
})

export default axiosURL;