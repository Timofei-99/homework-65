export const PAGES = [
    {title: 'About', id: 'about'},
    {title: 'Contacts', id: 'contacts'},
    {title: 'Customers', id: 'customers'},
    {title: 'Partners', id: 'partners'},
];