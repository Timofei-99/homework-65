import React from 'react';
import Toolbar from "../../Navigation/Toolbar/Toolbar";

const Layout = (props) => {
    return (
        <>
            <Toolbar/>
            <main className="Layout-Content">{props.children}</main>
        </>
    );
};

export default Layout;