import React from 'react';
import Logo from "../../UI/Logo/Logo";
import './Toolbar.css';
import Navigationitems from "../Navigationitems/Navigationitems";

const Toolbar = () => {
    return (
        <header className='head'>
            <div className="container Toolbar">
                <div>
                    <Logo />
                </div>
                <nav>
                    <Navigationitems/>
                </nav>
            </div>
        </header>
    );
};

export default Toolbar;