import React from 'react';
import {NavLink} from "react-router-dom";

const Navigationitem = ({to, exact, children}) => {
    return (
        <li>
            <NavLink to={to} exact={exact}>{children}</NavLink>
        </li>
    );
};

export default Navigationitem;