import React from 'react';
import Navigationitem from "./Navigationitem";
import './Navigationitems.css';
import {PAGES} from "../../../constbase";

const Navigationitems = () => {
    return (
        <ul className='Navigation'>
            {PAGES.map((p) => (
                <Navigationitem to={p.id} key={p.id}>
                    {p.title}
                </Navigationitem>
            ))}
            <Navigationitem to={'/pages/admin'}>Admin</Navigationitem>
        </ul>
    );
};

export default Navigationitems;